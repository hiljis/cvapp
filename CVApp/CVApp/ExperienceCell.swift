//
//  ExperienceCell.swift
//  CVApp
//
//  Created by Henric Hiljanen on 2018-12-07.
//  Copyright © 2018 HenricHiljanen. All rights reserved.
//

import UIKit

class ExperienceCell: UITableViewCell {
    
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeSpanLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
