//
//  Experience.swift
//  CVApp
//
//  Created by Henric Hiljanen on 2018-12-07.
//  Copyright © 2018 HenricHiljanen. All rights reserved.
//

import Foundation

class Experience {
    
    var imageName: String
    var title: String
    var timeSpan: String
    var description: String
    
    init () {
        self.imageName = ""
        self.title = ""
        self.timeSpan = ""
        self.description = ""
    }
    
    init (imageName: String, title: String, timeSpan: String, description: String) {
        self.imageName = imageName
        self.title = title
        self.timeSpan = timeSpan
        self.description = description
    }
    
}
